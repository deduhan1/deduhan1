﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Coin : MonoBehaviour
{
    public CoinGenerator generator;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.TryGetComponent<Hero>(out Hero hero))
        {
            generator.CoinPickedUp();
            Destroy(this);
        }
    }
}
