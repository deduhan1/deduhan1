﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Hero))]
public class PlayerKeyboard : MonoBehaviour
{
    private Hero hero;
    private void Awake()
    {
        hero = GetComponent<Hero>();
    }

    private void Update()
    {
        if (Input.GetKey(KeyCode.D))
        {
            hero.Move(Vector3.right);
        }
        if (Input.GetKey(KeyCode.A))
        {
            hero.Move(Vector3.left);
        }
        if (Input.GetKey(KeyCode.Space))
        {
            hero.Jump();
        }
        if (Input.GetKey(KeyCode.LeftShift))
        {
            hero.Run();
        }
    }
}
