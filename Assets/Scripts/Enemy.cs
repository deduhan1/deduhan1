﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor.Tilemaps;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    [SerializeField] private Transform _path;
    [SerializeField] private float _speed;

    private Transform[] _points;
    private int _currentPoint = 0;

    private void Awake()
    {
        _points = new Transform[_path == null ? 0 : _path.childCount];

        for (int i = 0; i < _points.Length; i++)
        {
            _points[i] = _path.GetChild(i);
        }
    }

    private void Update()
    {
        if (_points.Length > 0)
        {
            transform.position = Vector3.MoveTowards(transform.position, _points[_currentPoint].position, _speed * Time.deltaTime);

            if(transform.position == _points[_currentPoint].position)
            {
                _currentPoint++;

                if (_points.Length == _currentPoint)
                    _currentPoint = 0;
            }
        }
    }
}
