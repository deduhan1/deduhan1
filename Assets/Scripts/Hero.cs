﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Rigidbody2D))]
[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(Transform))]
public class Hero : MonoBehaviour
{
    [SerializeField] private float walkSpeed;
    [SerializeField] private float runSpeed;
    [SerializeField] private float jumpForse;

    private MoveState _moveState = MoveState.Idle;
    private DirectionState _directionState = DirectionState.Right;
    private Transform _transform;
    private Rigidbody2D _rigidbody2D;
    private Animator _animator;
    private float _moveTime = 0, _moveCooldown = 0.2f;

    private enum MoveState
    {
        Idle,
        Walk,
        Run,
        Jump
    }

    private enum DirectionState
    {
        Left,
        Right
    }

    private void Awake()
    {
        _transform = GetComponent<Transform>();
        _rigidbody2D = GetComponent<Rigidbody2D>();
        _animator = GetComponent<Animator>();
        _directionState = transform.localScale.x > 0 ? DirectionState.Right : DirectionState.Left;
    }

    private void FixedUpdate()
    {
        if (_moveState == MoveState.Jump)
        {
            if(_rigidbody2D.velocity == Vector2.zero)
            {
                Idle();
            }
        }
        else if(_moveState == MoveState.Walk || _moveState == MoveState.Run)
        {
            _rigidbody2D.velocity = ((_directionState == DirectionState.Right ? Vector2.right : -Vector2.right) *
                                     (_moveState == MoveState.Walk ? walkSpeed : runSpeed) * Time.fixedDeltaTime);
            _moveTime -= Time.fixedDeltaTime;
            if (_moveTime <= 0)
            {
                Idle();
                _rigidbody2D.velocity = Vector2.zero;
            }
        }

        if (_rigidbody2D.velocity.y < 0)
        {
            _moveState = MoveState.Jump;
        }
    }

    public void Idle()
    {
        _moveState = MoveState.Idle;
        _animator.Play("Idle");
    }

    public void Move(Vector3 playerDirection)
    {
        if(_moveState != MoveState.Jump)
        {
            Direction(playerDirection);
            if (_moveState == MoveState.Run)
                Run();
            else
                Walk();
            _moveTime = _moveCooldown;
        }
    }

    public void Run()
    {
        _moveState = MoveState.Run;
        _animator.Play("Run");
    }

    public void Walk()
    {
        _moveState = MoveState.Walk;
        _animator.Play("Walk");
    }

    public void Jump()
    {
        if(_moveState != MoveState.Jump)
        {
            _rigidbody2D.AddForce(Vector3.up * jumpForse, ForceMode2D.Impulse);
            _moveState = MoveState.Jump;
            _animator.Play("Jump");
        }
    }

    private void Direction(Vector3 direction)
    {
        if (_directionState == DirectionState.Right && direction == Vector3.left)
        {
            _transform.localScale = new Vector3(-transform.localScale.x, _transform.localScale.y, _transform.localScale.z);
            _directionState = DirectionState.Left;
        }

        if (_directionState == DirectionState.Left && direction == Vector3.right)
        {
            _transform.localScale = new Vector3(-transform.localScale.x, _transform.localScale.y, _transform.localScale.z);
            _directionState = DirectionState.Right;
        }
    }
}
