﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class House : MonoBehaviour
{
    [SerializeField] private Alarm _alarm;
    [SerializeField] private SpriteRenderer _sprite;

    private bool isEnemyInside = false;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (_alarm != null)
        {
            if (collision.TryGetComponent<Hero>(out Hero hero))
            {
                isEnemyInside = true;
                _alarm.On();
            }
        }

        StartCoroutine(ReduceAlpha(0.002f));
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if (_alarm != null)
        {
            if (collision.TryGetComponent<Hero>(out Hero hero))
            {
                isEnemyInside = false;
            }
            _alarm.Off();
        }

        StartCoroutine(IncreaseAlpha(0.002f));
    }


    private IEnumerator IncreaseAlpha(float _dessification)
    {
        var color = _sprite.color;

        while (_sprite.color.a != 1 && !isEnemyInside)
        {
            color.a += _dessification;
            _sprite.color = color;

            yield return null;
        }
    }

    private IEnumerator ReduceAlpha(float _dessification)
    {
        var color = _sprite.color;

        while (_sprite.color.a > 0.3f && isEnemyInside)
        {
            color.a -= _dessification;
            _sprite.color = color;

            yield return null;
        }
    }
}
