﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;

[RequireComponent(typeof(Transform))]
public class CoinGenerator : MonoBehaviour
{
    [SerializeField] private GameObject _coin;

    private Transform _transform;

    private void Awake()
    {
        _transform = GetComponent<Transform>();
    }

    private void Start()
    {
        StartCoroutine(CreateNewCoin(2));
    }

    private IEnumerator CreateNewCoin(float delay)
    {
        yield return new WaitForSeconds(delay);

        GameObject coin = _coin;
        coin.GetComponent<Coin>().generator = this;
        Instantiate(coin, _transform.position, Quaternion.identity);
    }

    public void CoinPickedUp()
    {
        StartCoroutine(CreateNewCoin(2));
    }
}
