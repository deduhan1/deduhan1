﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wallet : MonoBehaviour
{
    private int value;

    private void Start()
    {
        value = 0;
        PrintValue();
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.gameObject.TryGetComponent<Coin>(out Coin coin))
        {
            value++;
            Destroy(collision.gameObject);
            PrintValue();
        }
    }

    private void PrintValue()
    {
        Debug.Log("coins: " + value);
    }
}
