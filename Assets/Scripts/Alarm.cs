﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Alarm : MonoBehaviour
{
    [SerializeField] private float _volumeChange;

    private AudioSource _audio;

    private bool _isActivated = false;

    private void Awake()
    {
        _audio = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (_isActivated)
        {
            if (_audio.volume < 0.2f || _audio.volume == 1f)
            {
                _volumeChange *= -1;
            }

            _audio.volume -= _volumeChange * Time.deltaTime;
        }
    }

    public void On()
    {
        _audio.Play();
        _isActivated = true;
    }

    public void Off()
    {
        _audio.Stop();
        _isActivated = false;
    }
}
