﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyGenerator : MonoBehaviour
{
    [SerializeField] private GameObject _enemy;
    [SerializeField] private float _timeOfGeneration;
    [SerializeField] private List<GameObject> _generationPoints = new List<GameObject>();

    private float _timeToGenerate = 0;
    private int _currentPoint = 0;

    private void Update()
    {
        if(_generationPoints.Count != 0)
        {
            _timeToGenerate -= Time.deltaTime;
            if(_timeToGenerate <= 0)
            {
                Instantiate(_enemy, _generationPoints[_currentPoint].transform.position, Quaternion.identity);

                _currentPoint++;
                if(_currentPoint > _generationPoints.Count - 1)
                    _currentPoint = 0;
                _timeToGenerate = _timeOfGeneration;
            }
        }
    }
}
